Firebolt is the world’s fastest cloud data warehouse for data engineers, purpose-built for high-performance analytics. 
It provides sub-second query performance at terabyte to petabyte scale, at a fraction of the cost compared to alternatives. 
Companies adopting Firebolt have deployed high performance data analytics applications across internal BI as well as customer-facing use cases. 
Get started at [firebolt.io](https://www.firebolt.io/?utm_source=partner&utm_medium=referral&utm_campaign=keboola)

This component enables you to write data to a Firebolt table in a specified database.