import logging
import copy
from random import randint
from botocore.exceptions import ClientError as S3ClientError
from keboola.component.base import ComponentBase, UserException
from requests.exceptions import RetryError

from firebolt import FireboltClient, FireboltTableFactory, TableType, FireboltClientException
from s3 import S3Client
from typing import Any, Dict, List, Optional
from firebolt.index import PrimaryIndex
from firebolt.table import FireboltExternalTable, FireboltTable, FireboltFactTable, FireboltDimensionTable
from keboola.component.dao import TableDefinition

STAGING_TABLE_PREFIX = 'KBC_STAGING_'

KEY_FIREBOLT = "db"
KEY_FIREBOLT_USERNAME = 'user'
KEY_FIREBOLT_PASSWORD = '#password'
KEY_DATABASE_NAME = "database"
KEY_ENGINE_NAME = "engine_name"
KEY_REFRESH_TOKEN = "firebolt_refresh_token"

KEY_TABLES = "tables"
KEY_DB_TABLE_NAME = "dbName"
KEY_TABLE_TYPE = "table_type"
KEY_PRIMARY_INDEXES = "primaryIndex"
KEY_LOADING_OPTIONS = "loading_options"
KEY_LOAD_TYPE = "load_type"
KEY_APPEND_TIMESTAMP = "append_timestamp"
KEY_TABLE_AGGREGATION_INDEXES = "aggregation_indexes"
KEY_TABLE_JOIN_INDEXES = "join_indexes"
KEY_COLUMNS = "items"

KEY_AWS_API_KEY_ID = 'aws_api_key_id'
KEY_AWS_API_KEY_SECRET = '#aws_api_key_secret'
KEY_AWS_STAGING_BUCKET = "staging_bucket"
KEY_AWS_REGION = "aws_region"

KEY_POST_RUN_SCRIPTS = "post_run_scripts"
KEY_POST_RUN_RUN = "run"
KEY_CONTINUE_ON_FAILURE = "continue_on_failure"
KEY_SCRIPTS = "script"
CUSTOM_CUSTOM_OBJECTS = "custom_objects"

REQUIRED_PARAMETERS: List = []
REQUIRED_IMAGE_PARS: List = []

APPEND_TIMESTAMP_COLUMN_NAME = "upload_timestamp"


def handle_fb_exceptions(func):
    def func_wrapper(*args: Any, **kwargs: Any):
        try:
            return func(*args, **kwargs)
        except FireboltClientException as e:
            raise UserException(e) from e

    return func_wrapper


class Component(ComponentBase):
    def __init__(self) -> None:
        super().__init__(required_parameters=REQUIRED_PARAMETERS,
                         required_image_parameters=REQUIRED_IMAGE_PARS)

        params = self.configuration.parameters
        firebolt_params = params.get(KEY_FIREBOLT)
        firebolt_username = firebolt_params.get(KEY_FIREBOLT_USERNAME)
        firebolt_password = firebolt_params.get(KEY_FIREBOLT_PASSWORD)
        database_name = firebolt_params.get(KEY_DATABASE_NAME)
        engine_name = firebolt_params.get(KEY_ENGINE_NAME)
        self.firebolt_client = FireboltClient(database_name, firebolt_username, firebolt_password,
                                              engine_name=engine_name)

        aws_api_key_id = firebolt_params.get(KEY_AWS_API_KEY_ID)
        aws_api_key_secret = firebolt_params.get(KEY_AWS_API_KEY_SECRET)
        aws_region = firebolt_params.get(KEY_AWS_REGION)
        self.staging_bucket = firebolt_params.get(KEY_AWS_STAGING_BUCKET)
        self.s3_client = S3Client(aws_api_key_id, aws_api_key_secret, aws_region)

    def test_connection(self) -> None:
        self._init_clients()
        try:
            self.s3_client.login()
            self.s3_client.list_objects_in_bucket(self.staging_bucket)
        except S3ClientError as s3_client_error:
            raise UserException("Specified S3 credentials are not valid") from s3_client_error

    @handle_fb_exceptions
    def _init_clients(self) -> None:
        self.firebolt_client.login()
        self.s3_client.login()

    @handle_fb_exceptions
    def run(self) -> None:
        params = self.configuration.parameters
        self._init_clients()

        self.start_ingest_engine()

        view_name = params.get(KEY_DB_TABLE_NAME)

        table_type = TableType(params.get(KEY_TABLE_TYPE))

        loading_options = params.get(KEY_LOADING_OPTIONS)
        load_type = loading_options.get(KEY_LOAD_TYPE)
        input_columns = params.get(KEY_COLUMNS)
        input_columns = self.convert_column_types(input_columns)
        input_table_primary_indexes = params.get(KEY_PRIMARY_INDEXES, [])

        input_table = self.get_input_table()

        logging.info("Validating table settings")
        self.validate_table_settings(view_name, input_columns, table_type, input_table_primary_indexes)

        # Check if table exists for view name
        table = self.firebolt_client.get_table(view_name, table_type)
        if table:
            raise UserException("The table name you provided already exists in Firebolt. "
                                "This component generates views, which cannot have the "
                                "same names as existing tables in Firebolt")

        old_background_table = self.get_current_background_table(view_name, table_type)

        if load_type == "Delete from" and not old_background_table:
            raise UserException("Cannot Delete from table, the table does not exist")

        logging.info("Uploading Data to new background table")
        if load_type in ["Full load", "Incremental load"]:
            new_background_table = self.create_background_table_object(params, table_type)
        elif load_type == "Delete from":
            new_background_table = self.create_background_table_object(params, table_type, to_delete=True)
        else:
            raise UserException(f"Load type {load_type} not supported")
        self.upload_data_to_table(new_background_table, input_table)

        if load_type in ["Incremental load"] and old_background_table:
            logging.info("Performing Incremental Update")
            self.union_tables(new_background_table, old_background_table)

        if load_type == "Delete from":
            logging.info("Deleting rows from table")
            delete_background_table = copy.copy(new_background_table)
            new_background_table = self.create_background_table_object(params, table_type)
            self.firebolt_client.delete_from_table(new_background_table, delete_background_table, old_background_table)
            self.drop_table(delete_background_table)

        logging.info("Creating table indexes")
        self.create_table_indexes(new_background_table)

        view_exists = self.check_view_exists(view_name)

        if view_exists:
            logging.info("Dropping view")
            self.drop_view(view_name)

        logging.info("Recreating view")
        self.create_view(view_name, new_background_table.name)

        # delete old table
        if old_background_table:
            logging.info("Dropping old background table")
            self.drop_table(old_background_table)

        self.s3_client.clean_bucket(self.staging_bucket, prefix=STAGING_TABLE_PREFIX)

        post_run_scripts = params.get(KEY_POST_RUN_SCRIPTS, {})
        run = post_run_scripts.get(KEY_POST_RUN_RUN)
        if post_run_scripts and run:
            continue_on_failure = post_run_scripts.get(KEY_CONTINUE_ON_FAILURE)
            scripts = post_run_scripts.get(KEY_SCRIPTS)
            custom_script_objects = post_run_scripts.get(CUSTOM_CUSTOM_OBJECTS)
            self.run_post_run_scripts(scripts, continue_on_failure, view_name=view_name,
                                      background_table_name=new_background_table.name,
                                      custom_script_objects=custom_script_objects)

    def create_background_table_object(self, table_params: Dict, table_type: TableType,
                                       to_delete: bool = False) -> FireboltTable:
        background_table_name = self.generate_background_table_name(table_params["dbName"], to_delete=to_delete)

        updated_table_params = table_params.copy()
        updated_table_params["dbName"] = background_table_name

        # create background table
        table = self.create_table_object_from_params(updated_table_params, table_type)
        self.firebolt_client.create_table(table)

        # Add indexes to table object
        table = self.add_indexes_to_table(table, updated_table_params)

        return table

    def upload_data_to_table(self, table: FireboltTable, input_table: TableDefinition) -> None:
        s3_file_name = self.get_s3_staging_file_name(table.name)
        self.upload_data_to_s3(input_table, s3_file_name)

        # create external table
        logging.info("Creating External table")
        external_table = self.create_external_table_from_table(table, s3_file_name)

        # ingest to table
        logging.info("Ingesting data from external table to background table")
        self.firebolt_client.ingest_from_external_to_table(external_table, table)

        self.drop_table(external_table)

    @staticmethod
    def get_s3_staging_file_name(table_name: str) -> str:
        return "_".join([STAGING_TABLE_PREFIX, table_name])

    def get_input_table(self) -> TableDefinition:
        input_tables = self.get_input_tables_definitions()
        if len(input_tables) == 0:
            raise UserException("No input table added. Please add an input table")
        elif len(input_tables) > 1:
            raise UserException("Too many input tables added. Please add only one input table")
        return input_tables[0]

    @staticmethod
    def random_with_n_digits(n: int) -> int:
        range_start = 10 ** (n - 1)
        range_end = (10 ** n) - 1
        return randint(range_start, range_end)

    def start_ingest_engine(self) -> None:
        logging.info(f"Starting Ingest engine {self.firebolt_client.engine.name}, this might take a couple of minutes")
        self.firebolt_client.start_ingest_engine()
        logging.info(f"Ingest engine : {self.firebolt_client.engine.name} is ready")

    @staticmethod
    def validate_name(name, name_type):
        if len(name) < 1:
            raise UserException(f"{name_type} name must not be empty")
        if not name.replace('_', '').isalnum():
            raise UserException(f"{name_type} name '{name}' is invalid, "
                                "the name should only contain alphanumeric and underscore '_' characters")
        if not name[0].replace('_', '').isalpha():
            raise UserException(f"{name_type} '{name}' is invalid, "
                                "the first character must be a letter or underscore '_'")

    def validate_table_settings(self, table_view_name: str, input_columns: List[Dict], table_type: TableType,
                                input_table_primary_indexes: List[str]) -> None:

        self.validate_name(table_view_name, "Table/View")

        for input_column in input_columns:
            self.validate_name(input_column["dbName"], "Column")

        self.validate_primary_indexes(input_columns, input_table_primary_indexes)

        if table_type == TableType.Fact and not input_table_primary_indexes:
            raise UserException("Fact table must include primary index")

        firebolt_table_name = self.firebolt_client.get_table_name_from_view(table_view_name)
        if firebolt_table_name:
            firebolt_table = self.firebolt_client.get_table(firebolt_table_name, table_type)

        else:
            firebolt_table = None
        if firebolt_table:
            self.compare_columns(firebolt_table.columns, input_columns)
            self.compare_primary_indexes(firebolt_table.primary_indexes, input_table_primary_indexes)

    def compare_columns(self, db_table_columns: List[Dict], input_table_columns: List[Dict]) -> None:
        match = True
        issues = []
        if len(db_table_columns) != len(input_table_columns):
            raise UserException("Input table columns do not match the columns of the existing firebolt table"
                                f"Table in the database has {len(db_table_columns)} columns and the input table"
                                f"has {len(input_table_columns)} columns")

        input_table_column_names = [column["dbName"] for column in input_table_columns]

        for db_table_column in db_table_columns:
            if db_table_column["name"] not in input_table_column_names:
                issues.append(f"{db_table_column['name']} is Missing in input table")
                match = False
            else:
                matching_column = self.get_input_table_column_by_name(input_table_columns, db_table_column["name"])
                if matching_column and db_table_column["type"].lower() != matching_column["type"].lower():
                    issues.append(f"type of : {db_table_column['name']} ({db_table_column['type']}) "
                                  f"in db does not match the input : {matching_column['dbName']} "
                                  f"({matching_column['type']})")
                    match = False
        if not match:
            raise UserException("Input table columns do not match the columns of the existing firebolt table : "
                                f"{issues}")

    @staticmethod
    def get_input_table_column_by_name(input_table_columns: List[Dict], name: str) -> Optional[Dict]:
        for input_table_column in input_table_columns:
            if input_table_column['dbName'] == name:
                return input_table_column
        return None

    @staticmethod
    def compare_primary_indexes(firebolt_primary_indexes: PrimaryIndex, input_primary_indexes: List[str]) -> None:
        if set(input_primary_indexes) != set(firebolt_primary_indexes.columns):
            raise UserException(
                "Input table primary_indexes do not match the primary_indexes of the existing firebolt table")

    @handle_fb_exceptions
    def create_external_table_from_table(self, table: FireboltTable, input_csv_name: str) -> FireboltTable:
        external_table_name = "_".join(["ex", table.name])
        external_table = FireboltExternalTable(external_table_name)
        external_table.columns = table.columns
        self.firebolt_client.create_external_table(external_table, input_csv_name, self.staging_bucket,
                                                   self.s3_client.key_id, self.s3_client.key_secret)
        return external_table

    def add_indexes_to_table(self, table: FireboltTable, input_table_params: Dict) -> FireboltTable:
        if isinstance(table, FireboltFactTable):
            table = self.add_aggregation_indexes_to_fact_table(table, input_table_params)
        if isinstance(table, FireboltDimensionTable):
            table = self.add_join_indexes_to_dimension_table(table, input_table_params)
        return table

    def create_table_object_from_params(self, input_table_params: Dict[str, Any],
                                        table_type: TableType) -> FireboltTable:
        table = FireboltTableFactory.get_firebolt_table(input_table_params["dbName"], table_type)
        table.columns = self.get_input_table_columns(input_table_params)
        primary_index_columns = input_table_params.get(KEY_PRIMARY_INDEXES, [])
        table.set_primary_index(primary_index_columns)
        return table

    def add_aggregation_indexes_to_fact_table(self, fact_table: FireboltFactTable,
                                              input_table_params: Dict) -> FireboltFactTable:
        input_aggregation_indexes = input_table_params.get(KEY_TABLE_AGGREGATION_INDEXES, [])
        for input_aggregation_index in input_aggregation_indexes:
            index_name = self.generate_index_name(input_aggregation_index["index_name"])
            key_columns = input_aggregation_index["key_columns"]
            aggregations = input_aggregation_index["aggregations"]
            fact_table.add_aggregation_index(index_name, key_columns, aggregations)
        return fact_table

    def add_join_indexes_to_dimension_table(self, dimension_table: FireboltDimensionTable,
                                            input_table_params: Dict) -> FireboltDimensionTable:
        input_join_indexes = input_table_params.get(KEY_TABLE_JOIN_INDEXES, [])
        for input_join_index in input_join_indexes:
            index_name = self.generate_index_name(input_join_index["index_name"])
            join_index_column = input_join_index["join_index"]
            dimension_columns = input_join_index.get("dimension_columns")
            if not dimension_columns:
                dimension_columns = []
            dimension_table.add_join_index(index_name, join_index_column, dimension_columns)
        return dimension_table

    @staticmethod
    def get_input_table_columns(input_table_params: Dict) -> List[Dict]:
        columns = []
        for column in input_table_params[KEY_COLUMNS]:
            columns.append({"name": column["dbName"],
                            "type": column["type"],
                            "nullable": column["nullable"]})
        return columns

    def convert_column_types(self, input_columns: List[Dict]) -> List[Dict]:
        new_columns = []
        for column in input_columns:
            column["type"] = self.convert_keboola_datatype_to_firebolt_datatype(column["type"])
            new_columns.append(column)
        return new_columns

    @staticmethod
    def convert_keboola_datatype_to_firebolt_datatype(datatype: str) -> str:
        # this function is needed as datatype synonyms are automatically converted
        # eg. VARCHAR, TEXT, STRING are auto-converted to text
        if datatype.lower() in {"varchar", "string"}:
            return "text"
        elif datatype.lower() == "integer":
            return "int"
        elif datatype.lower() == "datetime":
            return "timestamp"
        else:
            return datatype.lower()

    def upload_data_to_s3(self, input_table: TableDefinition, table_name: str) -> None:
        try:
            self.s3_client.upload_to_bucket(table_name, input_table.full_path, self.staging_bucket)
        except S3ClientError as client_error:
            raise UserException(f"An error occurred when uploading data to S3 {client_error}") from client_error

    def get_run_id(self) -> int:
        return self.environment_variables.run_id or self.random_with_n_digits(8)

    def get_config_id(self) -> int:
        return self.environment_variables.config_id or self.random_with_n_digits(8)

    def generate_background_table_name(self, table_name: str, to_delete: bool = False) -> str:
        run_id = str(self.get_run_id()).split(".")[-1]
        if to_delete:
            return "_".join([table_name, str(self.get_config_id()), run_id, "to_delete"])
        return "_".join([table_name, str(self.get_config_id()), run_id, "background_table"])

    def generate_index_name(self, index_name: str) -> str:
        run_id = str(self.get_run_id()).split(".")[-1]
        return "_".join([index_name, str(self.get_config_id()), run_id])

    def drop_table(self, table: FireboltTable) -> None:
        self.drop_table_indexes(table)
        self._drop_table(table.name)

    @handle_fb_exceptions
    def union_tables(self, incremental_table: FireboltTable, old_table: FireboltTable) -> None:
        self.firebolt_client.union_tables(incremental_table, old_table)

    @handle_fb_exceptions
    def check_view_exists(self, view_name: str) -> bool:
        return self.firebolt_client.check_view_exists(view_name)

    @handle_fb_exceptions
    def drop_table_indexes(self, table: FireboltTable) -> None:
        self.firebolt_client.drop_table_indexes(table)

    @handle_fb_exceptions
    def _drop_table(self, table_name: str) -> None:
        self.firebolt_client.drop_table(table_name)

    @handle_fb_exceptions
    def drop_view(self, view_name: str) -> None:
        self.firebolt_client.drop_view(view_name)

    @handle_fb_exceptions
    def create_view(self, view_name: str, table_name: str) -> None:
        self.firebolt_client.create_view(view_name, table_name)

    @handle_fb_exceptions
    def create_table_indexes(self, table: FireboltTable) -> None:
        self.firebolt_client.update_table_indexes(table)

    @handle_fb_exceptions
    def get_current_background_table(self, view_name: str, table_type: TableType) -> Optional[FireboltTable]:
        current_background_table_name = self.firebolt_client.get_table_name_from_view(view_name)
        if current_background_table_name:
            return self.firebolt_client.get_table(current_background_table_name, table_type)
        return None

    def run_post_run_scripts(self, scripts: str,
                             continue_on_failure: bool,
                             view_name: str,
                             background_table_name: str,
                             custom_script_objects: Dict) -> None:
        logging.info("Running Post Run Scripts")
        scripts = scripts.split(';')
        whole_scripts = [script for script in scripts if len(script) > 4]
        for i, script in enumerate(whole_scripts):
            script = self.replace_script_object_names(script, view_name, background_table_name, custom_script_objects)
            logging.info(f"Running script #{i + 1}")
            try:
                self.firebolt_client.engine.run_sql(script)
                logging.info(f"Script #{i + 1} ran successfully")
            except (FireboltClientException, RetryError) as fb_exc:
                logging.info(f"Script #{i + 1} failed due to : {fb_exc}")
                if not continue_on_failure:
                    raise UserException(fb_exc) from fb_exc

    def replace_script_object_names(self, script: str,
                                    view_name: str,
                                    background_table_name: str,
                                    custom_script_objects: Dict) -> str:
        run_id = str(self.get_run_id()).split(".")[-1]
        dynamic_str = "_".join([str(self.get_config_id()), run_id])
        script = script.replace("{VIEW}", view_name)
        script = script.replace("{BACKGROUND_TABLE}", background_table_name)

        if custom_script_objects:
            l_c = "{"
            r_c = "}"
            for custom_object_name, value in custom_script_objects.items():

                dynamic_name = f"{l_c}OBJECT_DYNAMIC_{custom_object_name}{r_c}"
                non_dynamic_name = f"{l_c}OBJECT_{custom_object_name}{r_c}"
                if dynamic_name in script:
                    script = script.replace(dynamic_name, f"{value}_{dynamic_str}")
                if non_dynamic_name in script:
                    script = script.replace(non_dynamic_name, value)
        return script

    @staticmethod
    def validate_primary_indexes(input_columns: List, input_table_primary_indexes: List) -> None:
        nullable_error_columns = []
        for input_column in input_columns:
            if input_column["dbName"] in input_table_primary_indexes and input_column["nullable"]:
                nullable_error_columns.append(input_column["dbName"])
        if nullable_error_columns:
            raise UserException(
                f"Primary Indexes must not be nullable, columns {nullable_error_columns} are set as nullable.")


if __name__ == "__main__":
    try:
        comp = Component()
        comp.test_connection()
        comp.run()
    except UserException as exc:
        logging.exception(exc)
        exit(1)
    except Exception as exc:
        logging.exception(exc)
        exit(2)
