"""Firebolt client

Contains functions used for fetching access tokens to authorize the firebolt client
"""
import logging
from requests.exceptions import ConnectionError
from typing import Optional, List
from .engine import FireboltEngine
from .exceptions import FireboltValidationException, FireboltClientException
from .parser import get_aggregation_index_strings, parse_join_indexes
from .parser import get_columns_string, get_primary_indexes_string
from .parser import parse_table_columns, parse_primary_indexes, parse_aggregation_indexes
from .table import TableType, FireboltTableFactory, FireboltTable
from .index import IndexFactory

FIREBOLT_SCHEMA = "schema"
FIREBOLT_TABLE_NAME = "table_name"


class FireboltClient:
    def __init__(self, database_name: str, firebolt_user: str, firebolt_pass: str, engine_name: str = None):
        self._firebolt_user = firebolt_user
        self._firebolt_pass = firebolt_pass
        self.engine_name = engine_name
        self.engine = FireboltEngine(database_name)

    def login(self) -> None:
        try:
            self.engine.login(self._firebolt_user, self._firebolt_pass, self.engine_name)
        except ConnectionError as conn_exc:
            raise FireboltClientException(conn_exc) from conn_exc

    def start_ingest_engine(self) -> None:
        if not self.engine.get_engine_status():
            self.engine.start_engine()

    def stop_ingest_engine(self) -> None:
        self.engine.stop_engine()

    def check_view_exists(self, view_name: str) -> bool:
        response_data = self.get_table_name_from_view(view_name)
        if response_data:
            return True
        return False

    def get_table_name_from_view(self, view_name: str) -> Optional[str]:
        sql_script = f"SELECT * FROM information_schema.views WHERE table_name =  '{view_name}'"
        response_data = self.engine.run_sql(sql_script).get("data")
        if len(response_data) > 0:
            ddl = response_data[0].get("ddl")
            return ddl.split("FROM")[1].strip()
        return None

    def get_table(self, table_name: str, table_type: TableType) -> Optional[FireboltTable]:
        table = FireboltTableFactory.get_firebolt_table(table_name, table_type)

        columns = self.get_table_columns(table)
        table.set_column(columns)

        primary_indexes = self.get_table_primary_indexes(table)
        table.set_primary_index(primary_indexes)

        if table_type == TableType.Dimension:
            join_indexes = self.get_table_join_indexes(table)
            table.set_join_indexes(join_indexes)

        if table_type == TableType.Fact:
            aggregation_indexes = self.get_table_aggregation_indexes(table)
            table.set_aggregation_indexes(aggregation_indexes)

        if table.columns:
            return table
        return None

    def get_table_columns(self, table: FireboltTable) -> Optional[List]:
        sql_script = f"DESCRIBE {table.name}"
        response_dict = self.engine.run_sql(sql_script)
        if response_dict.get('data'):
            return parse_table_columns(response_dict.get('data'))
        return None

    def get_table_primary_indexes(self, table):
        get_index_script = f"show indexes {table.name}"
        response = self.engine.run_sql(get_index_script)
        return parse_primary_indexes(response.get("data")) if response.get("data") else []

    def get_table_join_indexes(self, table):
        index_data = self.fetch_indexes()
        join_indexes = parse_join_indexes(index_data)
        return [join_index for join_index in join_indexes if join_index.table_name == table.name]

    def get_table_aggregation_indexes(self, table):
        index_data = self.fetch_indexes()
        aggregation_indexes = parse_aggregation_indexes(index_data)
        return [aggregation_index for aggregation_index in aggregation_indexes if
                aggregation_index.table_name == table.name]

    def fetch_indexes(self):
        get_index_script = "show indexes"
        return self.engine.run_sql(get_index_script).get("data")

    def get_table_schema(self, table):
        sql_script = "SHOW TABLES"
        response = self.engine.run_sql(sql_script)
        for response_table in response.get("data"):
            if response_table[FIREBOLT_TABLE_NAME] == table.name:
                return response_table[FIREBOLT_SCHEMA]

    def drop_table(self, table_name):
        drop_script = f"DROP TABLE {table_name}"
        self.engine.run_sql(drop_script)

    def drop_join_index(self, join_index_name):
        drop_script = f"DROP join index {join_index_name}"
        self.engine.run_sql(drop_script)

    def drop_aggregating_index(self, aggregating_index_name):
        drop_script = f"DROP aggregating index {aggregating_index_name}"
        self.engine.run_sql(drop_script)

    def truncate_table(self, table_name):
        truncate_script = f"TRUNCATE {table_name}"
        self.engine.run_sql(truncate_script)

    def create_table_with_schema(self, table_schema):
        self.engine.run_sql(table_schema)

    def create_view(self, view_name, table_name):
        sql_script = f"CREATE VIEW {view_name} AS SELECT * FROM {table_name}"
        self.engine.run_sql(sql_script)

    def drop_view(self, view_name):
        sql_script = f"DROP VIEW {view_name}"
        self.engine.run_sql(sql_script)

    def create_table(self, table):
        columns_string = get_columns_string(table.columns)
        primary_indexes_string = get_primary_indexes_string(table.primary_indexes)
        sql_script = f"""
                CREATE {table.get_table_type()} TABLE {table.name} ( {columns_string} ) 
                {"PRIMARY INDEX" if table.primary_indexes.columns else ""} {primary_indexes_string};
                """  # noqa
        self.engine.run_sql(sql_script)

    def drop_table_indexes(self, table):
        if table.type == TableType.Fact:
            self._drop_table_aggregation_indexes(table)
        if table.type == TableType.Dimension:
            self._drop_table_join_indexes(table)

    def _drop_table_join_indexes(self, table):
        for join_index in table.join_indexes:
            self.drop_join_index(join_index.name)

    def _drop_table_aggregation_indexes(self, table):
        for aggregation_index in table.aggregation_indexes:
            self.drop_aggregating_index(aggregation_index.name)

    def update_table_indexes(self, table):
        if table.type == TableType.Fact:
            self._update_table_aggregation_indexes(table)
        if table.type == TableType.Dimension:
            self._update_table_join_indexes(table)

    def _update_table_aggregation_indexes(self, table):
        for aggregation_index in table.aggregation_indexes:
            index_exists, index_is_different = self.check_table_index(table, aggregation_index)
            if index_is_different and index_exists:
                logging.warning(f"Specified index {aggregation_index.name} is different from the one in Firebolt,"
                                f" dropping the one in Firebolt and creating a new one")
                self.drop_aggregating_index(aggregation_index.name)
                index_exists = False
            if not index_exists:
                self._create_aggregation_index(table, aggregation_index)

    def check_table_index(self, table, index):
        existing_index = self.get_index_by_name(index.name)
        index_exists, index_is_different = False, False

        if existing_index:
            index_exists = True
            index_is_different = existing_index != index

            if existing_index.table_name != table.name:
                raise FireboltValidationException(f"Index {index.name} specified for {table.name}, "
                                                  f"already exists for table {existing_index.table_name} "
                                                  f"choose a different name")

        return index_exists, index_is_different

    def get_index_by_name(self, index_name):
        index_data = self.fetch_indexes()
        for index_datum in index_data:
            if index_name == index_datum["index_name"]:
                return IndexFactory.get_index(index_datum)
        return None

    def _create_aggregation_index(self, table, aggregation_index):
        aggregation_indexes = get_aggregation_index_strings(aggregation_index.aggregations)
        aggregating_table_columns = aggregation_index.key_columns + aggregation_indexes
        aggregating_table_columns = ", ".join(aggregating_table_columns)

        sql_script = f"""CREATE AND GENERATE AGGREGATING INDEX {aggregation_index.name} 
                             ON {table.name} 
                             ( {aggregating_table_columns} );
                          """  # noqa
        self.engine.run_sql(sql_script)

    def _update_table_join_indexes(self, table):
        for join_index in table.join_indexes:
            index_exists, index_is_different = self.check_table_index(table, join_index)
            if index_is_different and index_exists:
                logging.warning(f"Specified index {join_index.name} is different from the one in Firebolt,"
                                f" dropping the one in Firebolt and creating a new one")
                self.drop_join_index(join_index.name)
                index_exists = False
            if not index_exists:
                self._create_join_index(table, join_index)

    def _create_join_index(self, table, join_index):
        join_columns = [join_index.join_index_column] + join_index.dimension_columns
        join_index_columns = self.get_column_list_string(join_columns)
        sql_script = f"CREATE AND GENERATE JOIN INDEX {join_index.name} ON {table.name} ({join_index_columns});"
        self.engine.run_sql(sql_script)

    def create_external_table(self, table, csv_name, s3_staging_bucket, aws_key_id, aws_key_secret):
        columns_string = get_columns_string(table.columns)
        external_table_url = "".join(["s3://", s3_staging_bucket, '/'])
        sql_script = f"""
            CREATE EXTERNAL TABLE {table.name} 
            ( {columns_string} ) 
            URL = '{external_table_url}' 
            CREDENTIALS = ( AWS_KEY_ID = '{aws_key_id}' AWS_SECRET_KEY = '{aws_key_secret}')  
            OBJECT_PATTERN= '{csv_name}'
            TYPE = (CSV SKIP_HEADER_ROWS=1); """  # noqa
        self.engine.run_sql(sql_script)

    def ingest_from_external_to_table(self, external_table, table):
        sql_script = f"""
                   INSERT INTO {table.name}
                   SELECT *
                   FROM   {external_table.name};  
                   """  # noqa
        self.engine.run_sql(sql_script)

    def ingest_from_table_to_table(self, ingesting_table, final_table):
        ingest_script = f"INSERT INTO {final_table.name} SELECT * FROM   {ingesting_table.name};"
        self.engine.run_sql(ingest_script)

    def download_table(self, table_name):
        download_data_script = f"SELECT * FROM {table_name};"
        return self.engine.run_sql(download_data_script).get("data")

    def download_table_descriptions(self):
        download_data_script = "Show tables;"
        return self.engine.run_sql(download_data_script).get("data")

    def download_view_descriptions(self):
        download_data_script = "SELECT * FROM information_schema.views;"
        return self.engine.run_sql(download_data_script).get("data")

    def download_table_indexes(self):
        download_data_script = "Show indexes;"
        return self.engine.run_sql(download_data_script).get("data")

    def union_tables(self, incremental_table, old_table):
        id_columns = self.get_column_list_string(incremental_table.primary_indexes.columns, incremental_table.columns)
        if len(incremental_table.primary_indexes.columns) > 1:
            union_script = f"""insert into {incremental_table.name}  
                               SELECT * from {old_table.name} 
                               where CONCAT({id_columns}) NOT IN 
                               (SELECT CONCAT({id_columns}) FROM {incremental_table.name})"""  # noqa
        else:
            union_script = f"""insert into {incremental_table.name}  
                               SELECT * from {old_table.name} 
                               where {id_columns} NOT IN 
                               (SELECT {id_columns} FROM {incremental_table.name})"""  # noqa
        self.engine.run_sql(union_script)

    def delete_from_table(self, new_table, table_with_data_to_delete, old_table):
        id_columns = self.get_column_list_string(new_table.primary_indexes.columns, new_table.columns)
        if len(new_table.primary_indexes.columns) > 1:
            union_script = f"""insert into {new_table.name}  
                                       SELECT * from {old_table.name} 
                                       where CONCAT({id_columns}) NOT IN 
                                       (SELECT CONCAT({id_columns}) FROM {table_with_data_to_delete.name})"""  # noqa
        else:
            union_script = f"""insert into {new_table.name}  
                                       SELECT * from {old_table.name} 
                                       where {id_columns} NOT IN 
                                       (SELECT {id_columns} FROM {table_with_data_to_delete.name})"""  # noqa
        self.engine.run_sql(union_script)

    @staticmethod
    def get_column_list_string(primary_columns, all_columns):
        col_names = []
        for column in primary_columns:
            col_type = [col["type"] for col in all_columns if col["name"] == column][0]
            if col_type != "text":
                col_names.append(f"CAST(\"{column}\" AS TEXT)")
            else:
                col_names.append(f"\"{column}\"")
        return ",".join(col_names)
