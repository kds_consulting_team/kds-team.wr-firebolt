from .index import PrimaryIndex, AggregationIndex, JoinIndex
from enum import Enum

FIREBOLT_INDEX_TYPE = "type"


class TableType(Enum):
    Fact = "Fact"
    Dimension = "Dimension"
    External = "External"
    DEFAULT = ""


class FireboltTable:
    def __init__(self, table_name):
        self.name = table_name
        self.exists = False
        self.columns = []
        self.primary_indexes = []
        self.type = TableType.DEFAULT

    def get_table_type(self):
        return self.type.name

    def set_column(self, columns):
        self.columns = columns

    def set_primary_index(self, columns):
        index_name = "_".join(["primary", self.name])
        self.primary_indexes = PrimaryIndex(index_name, self.name, columns)


class FireboltFactTable(FireboltTable):
    def __init__(self, table_name):
        super().__init__(table_name)
        self.aggregation_indexes = []
        self.type = TableType.Fact

    def set_aggregation_indexes(self, aggregation_indexes):
        self.aggregation_indexes = aggregation_indexes

    def add_aggregation_index(self, index_name, key_columns, aggregations):
        new_aggregation_index = AggregationIndex(index_name, self.name, key_columns, aggregations)
        for i, aggregation_index in enumerate(self.aggregation_indexes):
            if aggregation_index.name == new_aggregation_index.name and \
                    aggregation_index.table_name == new_aggregation_index.table_name and \
                    aggregation_index != new_aggregation_index:
                self.aggregation_indexes[i] = new_aggregation_index
                return
        self.aggregation_indexes.append(new_aggregation_index)


class FireboltDimensionTable(FireboltTable):
    def __init__(self, table_name):
        super().__init__(table_name)
        self.primary_indexes = []
        self.join_indexes = []
        self.type = TableType.Dimension

    def set_join_indexes(self, join_indexes):
        self.join_indexes = join_indexes

    def add_join_index(self, index_name, join_index_column, dimension_columns):
        join_index = JoinIndex(index_name, self.name, join_index_column, dimension_columns)
        self.join_indexes.append(join_index)


class FireboltExternalTable(FireboltTable):
    def __init__(self, table_name):
        super().__init__(table_name)
        self.type = TableType.External


class FireboltTableFactory:
    @staticmethod
    def get_firebolt_table(table_name, table_type):
        if table_type == TableType.Fact:
            return FireboltFactTable(table_name)
        if table_type == TableType.Dimension:
            return FireboltDimensionTable(table_name)
