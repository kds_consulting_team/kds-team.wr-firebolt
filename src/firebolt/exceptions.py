class FireboltClientException(Exception):
    pass


class FireboltValidationException(Exception):
    pass
