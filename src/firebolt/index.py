class FireboltIndex:
    def __init__(self, name, table_name):
        self.name = name
        self.table_name = table_name


class PrimaryIndex(FireboltIndex):
    def __init__(self, name, table_name, columns):
        super().__init__(name, table_name)
        self.columns = columns


class AggregationIndex(FireboltIndex):
    def __init__(self, name, table_name, key_columns, aggregations):
        super().__init__(name, table_name)
        self.key_columns = key_columns
        self.aggregations = aggregations

    def __eq__(self, other):
        if not isinstance(other, AggregationIndex):
            return False
        if self.name != other.name:
            return False
        if len(self.key_columns) != len(other.key_columns):
            return False
        if set(self.key_columns) != set(other.key_columns):
            return False
        if len(self.aggregations) != len(other.aggregations):
            return False
        for self_aggregation, other_aggregation in zip(self.aggregations, other.aggregations):
            # TODO FIX AGGREGATIONS
            # if self_aggregation["distinct"] != other_aggregation["distinct"]:
            #     return False
            if self_aggregation["column"].strip() != other_aggregation["column"].strip():
                return False
            if self_aggregation["function"].strip() != other_aggregation["function"].strip():
                return False
        return True


class JoinIndex(FireboltIndex):
    def __init__(self, name, table_name, join_index_column, dimension_columns):
        super().__init__(name, table_name)
        self.join_index_column = join_index_column
        self.dimension_columns = dimension_columns

    def __eq__(self, other):
        if not isinstance(other, JoinIndex):
            return False
        if self.name != other.name:
            return False
        if len(self.dimension_columns) != len(other.dimension_columns):
            return False
        if set(self.dimension_columns) != set(other.dimension_columns):
            return False
        if self.join_index_column != other.join_index_column:
            return False
        return True


class IndexFactory:
    @staticmethod
    def get_index(index_data):
        if index_data["type"] == "primary":
            return PrimaryIndex(index_data["index_name"], index_data["table_name"], index_data["expression"])
        if index_data["type"] == "aggregating":
            # TODO use parser
            expressions = [expression.replace("\"", "") for expression in index_data["expression"]]
            key_columns = IndexFactory.get_columns_from_expressions(expressions)
            aggregations = IndexFactory.get_aggregations_from_expressions(expressions)
            return AggregationIndex(index_data["index_name"], index_data["table_name"], key_columns, aggregations)
        if index_data["type"] == "join":
            # TODO use parser
            expressions = [expression.replace("\"", "") for expression in index_data["expression"]]
            join_index_column = expressions[0]
            dimension_columns = expressions[1:] if len(expressions) > 1 else []
            return JoinIndex(index_data["index_name"], index_data["table_name"], join_index_column, dimension_columns)

    # TODO use parser
    @staticmethod
    def get_aggregations_from_expressions(expressions):
        aggregations = []
        for expression in expressions:
            if "(" in expression:
                distinct = False
                if "distinct" in expression.lower():
                    distinct = True
                words = expression.replace(")", "").split("(")
                function = words[0]
                column = words[1].lower().replace("distinct", "")
                aggregations.append({
                    "column": column,
                    "function": function,
                    "distinct": distinct
                })
        return aggregations

    # TODO use parser
    @staticmethod
    def get_columns_from_expressions(expressions):
        columns = []
        for expression in expressions:
            if "(" not in expression:
                columns.append(expression)
        return columns
