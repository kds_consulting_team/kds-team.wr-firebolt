import json
from .index import AggregationIndex, JoinIndex
from .exceptions import FireboltClientException

FIREBOLT_TABLE_NAME = "table_name"
FIREBOLT_COLUMN_NAME = "column_name"
FIREBOLT_DATA_TYPE = "data_type"
FIREBOLT_NULLABLE = "nullable"
FIREBOLT_INDEX_TYPE = "type"
FIREBOLT_SCHEMA = "schema"

PRIMARY = "primary"
AGGREGATING = "aggregating"
JOIN = "join"


def parse_response(response):
    if response.status_code == 200:
        response_dict = json.loads(response.text)
    else:
        raise FireboltClientException(response.text)
    return response_dict


def parse_table_columns(table_data):
    columns = []
    for column in table_data:
        columns.append({"name": column[FIREBOLT_COLUMN_NAME],
                        "type": column[FIREBOLT_DATA_TYPE],
                        "nullable": column[FIREBOLT_NULLABLE]})
    return columns


def get_aggregation_index_strings(aggregation_indexes):
    aggregation_index_strings = []
    for aggregation_index in aggregation_indexes:
        aggregation_function = aggregation_index["function"]
        aggregation_column = aggregation_index["column"]
        distinct = aggregation_index.get("distinct", False)
        aggregation_index_string = "".join(
            [aggregation_function, "(", "distinct " if distinct else "", aggregation_column, ")"])
        aggregation_index_strings.append(aggregation_index_string)
    return aggregation_index_strings


def get_aggregations_from_expressions(expressions):
    aggregations = []
    for expression in expressions:
        if "(" in expression:
            distinct = False
            if "distinct" in expression.lower():
                distinct = True
            words = expression.replace(")", "").split("(")
            function = words[0]
            column = words[1].lower().replace("distinct", "")
            aggregations.append({
                "column": column,
                "function": function,
                "distinct": distinct
            })
    return aggregations


def get_columns_from_expressions(expressions):
    return [expression for expression in expressions if "(" not in expression]


def parse_primary_indexes(index_data):
    primary_indexes = []
    for index in index_data:
        if index[FIREBOLT_INDEX_TYPE] == PRIMARY:
            index_columns = index["expression"]
            primary_indexes.extend(index_columns)
    return primary_indexes


def parse_aggregation_indexes(index_data):
    aggregation_indexes = []
    for index in index_data:
        if index[FIREBOLT_INDEX_TYPE] == AGGREGATING:
            index_name = index["index_name"]
            key_columns, aggregations = parse_aggregation_expression(index)
            aggregation_indexes.append(AggregationIndex(index_name, index["table_name"], key_columns, aggregations))
    return aggregation_indexes


def parse_aggregation_expression(index_data):
    expressions = [expression.replace("\"", "") for expression in index_data["expression"]]
    key_columns = get_columns_from_expressions(expressions)
    aggregations = get_aggregations_from_expressions(expressions)
    return key_columns, aggregations


def parse_join_indexes(indexes):
    join_indexes = []
    for index in indexes:
        if index[FIREBOLT_INDEX_TYPE] == JOIN:
            index_name = index["index_name"]
            join_index_column, dimension_columns = parse_join_expression(index)
            join_indexes.append(JoinIndex(index_name, index["table_name"], join_index_column, dimension_columns))
    return join_indexes


def parse_join_expression(index_data):
    expressions = [expression.replace("\"", "") for expression in index_data["expression"]]
    join_index_column = expressions[0]
    dimension_columns = expressions[1:] if len(expressions) > 1 else []
    return join_index_column, dimension_columns


def get_primary_indexes_string(primary_indexes):
    primary_index_strs = [f"\"{primary_index}\"" for primary_index in primary_indexes.columns]
    return ", ".join(primary_index_strs)


def get_columns_string(input_columns):
    column_strings = []
    for input_column in input_columns:
        not_null_str = "NULL" if input_column["nullable"] else "NOT NULL"
        col_str = "".join(["\"", input_column["name"], "\""])
        column_strings.append(" ".join([col_str, input_column["type"], not_null_str]))
    return ", ".join(column_strings)
