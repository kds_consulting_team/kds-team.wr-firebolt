import time
import requests
import logging
from requests.exceptions import ConnectionError
from typing import Dict
from keboola.http_client import HttpClient
from .auth import get_authentication_token
from .exceptions import FireboltClientException
from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry

CORE_URL = "https://api.app.firebolt.io/core/v1"
ACCOUNT_ENDPOINT = "/account"
ACCOUNTS_ENDPOINT = "/accounts"
ENGINES_ENDPOINT = "/engines"
DATABASES_ENDPOINT = "/databases"
BINDINGS_ENDPOINT = "/bindings"
IAM_URL = "https://api.app.firebolt.io/iam/v2"


class FireboltEngine(HttpClient):
    def __init__(self, database_name: str):
        self.database_name = database_name
        self.authorization_header = None
        self.url = None
        self.name = None
        self.id = None
        self.account_id = None
        self.firebolt_user = None
        super().__init__(CORE_URL, max_retries=3)

    @staticmethod
    def _construct_auth_header(access_token):
        return {'Authorization': f'Bearer {access_token}'}

    def login(self, firebolt_user: str, firebolt_pass: str, engine_name: str = None) -> None:
        refresh_token, access_token = get_authentication_token(firebolt_user, firebolt_pass)
        authorization_header = self._construct_auth_header(access_token)
        self.firebolt_user = firebolt_user
        self.authorization_header = authorization_header
        self.account_id = self.get_account_id()

        if engine_name:
            self.id = self.get_engine_id_by_name(engine_name)
            self.check_engine_and_database_binding(engine_name, self.database_name)
        else:
            self.id = self.get_default_engine_of_database(self.database_name)
        self.url = self.get_engine_url_by_id(self.id)
        self.name = self.get_engine_name_from_url(self.url)

    def get_account_id(self) -> str:
        account_url = "".join([IAM_URL, ACCOUNT_ENDPOINT])
        response = self.get(endpoint_path=account_url, is_absolute_path=True, headers=self.authorization_header)
        try:
            account_id = response.get("account").get("id")
        except AttributeError:
            raise FireboltClientException("No Account ID was found for your login information")
        return account_id

    def get_default_engine_of_database(self, database_name: str) -> str:
        database_id = self.get_database_id(database_name)
        engine_id = self.get_default_engine_of_database_id(database_id)
        return engine_id

    def get_engine_id_by_name(self, engine_name: str) -> str:
        engines_url = "".join(
            [CORE_URL, ACCOUNTS_ENDPOINT, "/", self.account_id, ENGINES_ENDPOINT])
        response = self.get(endpoint_path=engines_url, is_absolute_path=True, headers=self.authorization_header)
        try:
            edges = response.get("edges")
            engine_data = [edge for edge in edges if edge.get("node").get("name") == engine_name][0]
            engine_id = engine_data.get("node").get("id").get("engine_id")
        except (AttributeError, IndexError):
            raise FireboltClientException(f"Could not find engine :{engine_name}")
        return engine_id

    def check_engine_and_database_binding(self, engine_name: str, database_name: str) -> None:
        database_id = self.get_database_id(database_name)
        engine_id = self.get_engine_id_by_name(engine_name)
        bindings_url = "".join(
            [CORE_URL, ACCOUNTS_ENDPOINT, "/", self.account_id, BINDINGS_ENDPOINT])
        response = self.get(endpoint_path=bindings_url, is_absolute_path=True, headers=self.authorization_header)
        edges = response.get("edges")
        binding = [edge for edge in edges if edge.get("node").get("id").get("engine_id") == engine_id][0]
        if binding.get("node").get("id").get("database_id") != database_id:
            raise FireboltClientException(
                f"Engine :{engine_name} is not bound to {database_name}, please make sure it is")

    def get_database_id(self, database_name: str) -> str:
        parameters = {"database_name": database_name}
        database_url = "".join(
            [CORE_URL, ACCOUNTS_ENDPOINT, "/", self.account_id, DATABASES_ENDPOINT, ":getIdByName"])
        response = self.get(endpoint_path=database_url, is_absolute_path=True, headers=self.authorization_header,
                            params=parameters)
        try:
            database_id = response.get("database_id").get("database_id")
        except AttributeError:
            raise FireboltClientException(
                f"Database {database_name} is not linked to your account "
                f"{self.firebolt_user} with account id {self.account_id}")
        return database_id

    def get_default_engine_of_database_id(self, database_id: str) -> str:
        parameters = {"filter.id_database_id_eq": database_id}
        bindings_url = "".join(
            [CORE_URL, ACCOUNTS_ENDPOINT, "/", self.account_id, BINDINGS_ENDPOINT])
        response = self.get(endpoint_path=bindings_url, is_absolute_path=True, headers=self.authorization_header,
                            params=parameters)
        try:
            edges = response.get("edges")
            default_binding = [edge for edge in edges if edge.get("node").get("engine_is_default")][0]
            default_engine_id = default_binding.get("node").get("id").get("engine_id")
        except (AttributeError, IndexError):
            raise FireboltClientException(f"Could not find default engine for database : {self.database_name}")
        return default_engine_id

    def run_sql(self, sql_script: str) -> requests.Response:
        logging.info(f"Running SQL script : {sql_script}")
        headers = self.authorization_header
        params = {'database': self.database_name}
        request_url = "".join(["https://", self.url, ":443"])
        headers["Content-Type"] = 'text/plain'
        try:
            response = self.post_raw(endpoint_path=request_url,
                                     is_absolute_path=True,
                                     headers=headers,
                                     params=params,
                                     data=sql_script)  # noqa
        except ConnectionError as conn_err:
            raise FireboltClientException(conn_err) from conn_err
        self._handle_http_error(response)
        try:
            response_data = response.json()
        except requests.exceptions.JSONDecodeError:
            raise FireboltClientException(f"Failed to process Firebolt response :{response.text}")
        return response_data

    def get_engine_url_by_id(self, engine_id: str) -> str:
        engine_url = "".join(
            [CORE_URL, ACCOUNTS_ENDPOINT, "/", str(self.account_id), ENGINES_ENDPOINT, "/", engine_id])
        response = self.get(endpoint_path=engine_url, is_absolute_path=True, headers=self.authorization_header)
        engine_url = response.get("engine").get("endpoint")
        return engine_url

    def start_engine(self) -> None:
        headers = self.authorization_header
        start_engine_url = "".join(
            [CORE_URL, ACCOUNTS_ENDPOINT, "/", str(self.account_id), ENGINES_ENDPOINT, "/", str(self.id), ":start"])
        self.post(endpoint_path=start_engine_url, is_absolute_path=True, headers=headers)
        engine_on = False
        while not engine_on:
            engine_on = self.get_engine_status()
            time.sleep(10)

    def stop_engine(self) -> None:
        headers = self.authorization_header
        stop_engine_url = "".join(
            [CORE_URL, ACCOUNTS_ENDPOINT, "/", str(self.account_id), ENGINES_ENDPOINT, "/", str(self.id), ":stop"])
        self.post(endpoint_path=stop_engine_url, is_absolute_path=True, headers=headers)

    @staticmethod
    def get_engine_name_from_url(engine_url: str) -> str:
        return engine_url.split(".")[0].replace("-", "_")

    def get_engine_id(self, url, authorization_header, database_name):
        engine = self.get_engine_data(url, authorization_header, database_name)
        return engine["node"]["id"]["engine_id"]

    def get_engine_status(self) -> bool:
        engine = self.get_engine_data(self.url, self.authorization_header, self.database_name)
        if engine["node"]["current_status_summary"] == 'ENGINE_STATUS_SUMMARY_RUNNING':
            return True
        else:
            return False

    def get_engine_data(self, url: str, authorization_header: Dict, database_name: str) -> Dict:
        headers = authorization_header
        params = {'database_name': database_name}
        engine_url = "".join([CORE_URL, ACCOUNTS_ENDPOINT, "/", str(self.account_id), ENGINES_ENDPOINT])
        response = self.get(endpoint_path=engine_url, is_absolute_path=True, headers=headers, params=params)
        engines = response.get("edges")
        for engine in engines:
            if engine["node"]["endpoint"] == url:
                return engine

    @staticmethod
    def _handle_http_error(response):
        try:
            response.raise_for_status()
        except requests.HTTPError as e:
            raise FireboltClientException(e.response.text) from e

    # override to continue on failure
    def _requests_retry_session(self, session=None):
        session = session or requests.Session()
        retry = Retry(
            total=self.max_retries,
            read=self.max_retries,
            connect=self.max_retries,
            backoff_factor=self.backoff_factor,
            status_forcelist=self.status_forcelist
        )
        adapter = HTTPAdapter(max_retries=retry)
        session.mount('http://', adapter)
        session.mount('https://', adapter)
        return session
