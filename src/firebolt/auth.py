"""Authorization

Contains functions used for fetching access tokens to authorize the firebolt client
"""

from keboola.http_client import HttpClient
from typing import Tuple

AUTH_URL = 'https://api.app.firebolt.io/auth/v1'
LOGIN_ENDPOINT = "login"
REFRESH_ENDPOINT = 'refresh'


def get_authentication_token(firebolt_user: str, firebolt_pass: str) -> Tuple[str, str]:
    headers = {'Content-Type': 'application/json;charset=UTF-8'}
    data = {"username": firebolt_user, "password": firebolt_pass}
    auth_client = HttpClient(AUTH_URL)
    response = auth_client.post(endpoint_path=LOGIN_ENDPOINT, headers=headers, json=data)
    refresh_token = response.get("refresh_token")
    access_token = response.get("access_token")
    return refresh_token, access_token


def refresh_access_token(refresh_token: str) -> str:
    headers = {'Content-Type': 'application/json;charset=UTF-8'}
    data = {"refresh_token": refresh_token}
    auth_client = HttpClient(AUTH_URL)
    response = auth_client.post(endpoint_path=REFRESH_ENDPOINT, headers=headers, json=data)
    access_token = response.get("access_token")
    return access_token
