import boto3
import logging
from botocore.exceptions import ClientError
from typing import List


class S3ClientException(Exception):
    pass


class S3Client:
    def __init__(self, key_id: str, key_secret: str, aws_region: str):
        self.key_id = key_id
        self.key_secret = key_secret
        self.aws_region = aws_region
        self.client = None
        self.resource = None

    def login(self):
        self.client = boto3.client('s3', aws_access_key_id=self.key_id, aws_secret_access_key=self.key_secret,
                                   region_name=self.aws_region)
        self.resource = boto3.resource('s3', aws_access_key_id=self.key_id, aws_secret_access_key=self.key_secret,
                                       region_name=self.aws_region)

    def upload_to_bucket(self, input_table_name: str, input_table_path: str, bucket_name: str) -> None:
        if self.client:
            with open(input_table_path, "rb") as f:
                self.client.upload_fileobj(f, bucket_name, input_table_name)
        else:
            raise S3ClientException("Log in to client before performing actions")

    def clean_bucket(self, bucket_name: str, prefix: str = 'KBC_STAGING_') -> None:
        if not self.client:
            raise S3ClientException("Log in to client before performing actions")
        try:
            bucket = self.resource.Bucket(bucket_name)
            bucket.objects.filter(Prefix=prefix).delete()
        except ClientError as e:
            logging.error(e)

    def check_if_bucket_exists(self, bucket_name: str) -> bool:
        if not self.client:
            raise S3ClientException("Log in to client before performing actions")
        if self.resource.Bucket(bucket_name) in self.resource.buckets.all():
            return True
        return False

    def list_objects_in_bucket(self, bucket_name: str) -> List:
        if not self.client:
            raise S3ClientException("Log in to client before performing actions")
        bucket = self.resource.Bucket(bucket_name)
        objects_in_bucket = []
        for my_bucket_object in bucket.objects.all():
            objects_in_bucket.append(my_bucket_object.key)
        return objects_in_bucket
