import json
import os
import unittest
import logging
import csv
from operator import itemgetter
from random import randint
from datadirtest import DataDirTester, TestDataDir
from keboola.component import CommonInterface

from src.firebolt import FireboltClient
import warnings


class FireboltWriterIntegrationTest(TestDataDir):
    def run_component(self):
        os.environ['KBC_RUNID'] = "xx" + str(self.random_with_n_digits(8)) + "xx"
        self.ci.environment_variables.run_id = "xx" + str(self.random_with_n_digits(8)) + "xx"
        super().run_component()
        self.download_result_tables()

    def download_result_tables(self):
        fb_client = FireboltClient(firebolt_user=os.environ['FIREBOLT_USER_NAME'],
                                   firebolt_pass=os.environ['FIREBOLT_PASSWORD'],
                                   database_name=os.environ['FIREBOLT_TEST_DB_NAME'])
        fb_client.login()

        out_tables_path = self.ci.tables_out_path
        config_dict = self.ci.configuration.config_data
        table_name = config_dict["parameters"]["dbName"]
        table_data = fb_client.download_table(table_name)

        print()
        print("downloading result tables")
        print(table_name)
        try:
            run_id = fb_client.download_table_descriptions()[0]["table_name"].split("xx")[1]
        except IndexError:
            run_id = ""
        print(f"run_id : {run_id}")
        run_id = str(run_id)

        if not os.path.exists(out_tables_path):
            os.makedirs(out_tables_path)

        table_data_sorted = sorted(table_data, key=itemgetter('first_name'))
        self.write_data_to_file(os.path.join(out_tables_path, "table_data.csv"), table_data_sorted)
        table_description_data = fb_client.download_table_descriptions()
        for i, description in enumerate(table_description_data):
            for key in table_description_data[i]:
                if isinstance(table_description_data[i][key], str):
                    table_description_data[i][key] = table_description_data[i][key].replace(run_id, "")

        self.write_data_to_file(os.path.join(out_tables_path, "table_description_data.csv"),
                                table_description_data,
                                columns=["table_name", "state", "table_type", "column_count", "primary_index", "schema",
                                         "number_of_rows"])

        index_data = fb_client.download_table_indexes()
        for i, description in enumerate(index_data):
            for key in index_data[i]:
                if isinstance(index_data[i][key], str):
                    index_data[i][key] = index_data[i][key].replace(run_id, "")
        self.write_data_to_file(os.path.join(out_tables_path, "index_data.csv"),
                                index_data,
                                columns=["index_name", "table_name", "type", "expression"])

    @staticmethod
    def write_data_to_file(file_name, data, columns=None):
        with open(file_name, 'w') as csvfile:
            fieldnames = list(data[0].keys())
            if columns:
                fieldnames = columns
            writer = csv.DictWriter(csvfile, fieldnames=fieldnames, extrasaction='ignore')
            writer.writeheader()
            for datum in data:
                writer.writerow(datum)

    def _inject_firebolt_credentials(self):
        config_dict = self.ci.configuration.config_data
        firebolt_user = os.environ['FIREBOLT_USER_NAME']
        firebolt_pass = os.environ['FIREBOLT_PASSWORD']
        database_name = os.environ['FIREBOLT_TEST_DB_NAME']
        staging_bucket = os.environ['FIREBOLT_STAGING_BUCKET']
        aws_id = os.environ['AWS_API_KEY_ID']
        aws_secret = os.environ['AWS_API_KEY_SECRET']
        fb_driver = os.environ['FIREBOLT_DRIVER']
        aws_region = os.environ['AWS_REGION']

        config_dict["parameters"]['db']['user'] = firebolt_user
        config_dict['parameters']['db']['#password'] = firebolt_pass
        config_dict['parameters']['db']['database'] = database_name
        config_dict['parameters']['db']['staging_bucket'] = staging_bucket
        config_dict['parameters']['db']['aws_api_key_id'] = aws_id
        config_dict['parameters']['db']['#aws_api_key_secret'] = aws_secret
        config_dict['parameters']['db']['driver'] = fb_driver
        config_dict['parameters']['db']['aws_region'] = aws_region

        with open(os.path.join(self.ci.data_folder_path, 'config.json'), 'w+') as cfg_out:
            json.dump(config_dict, cfg_out)

    def setUp(self):
        """
        Executes before test.
        """

        super().setUp()
        os.environ['KBC_CONFIGID'] = "2"

        print()
        print()
        print()
        print(f"Setup")
        print()
        print()
        print()

        self.ci = CommonInterface(data_folder_path=os.path.join(self.data_dir, "source", "data"))

        self._inject_firebolt_credentials()

        fb_client = FireboltClient(firebolt_user=os.environ['FIREBOLT_USER_NAME'],
                                   firebolt_pass=os.environ['FIREBOLT_PASSWORD'],
                                   database_name=os.environ['FIREBOLT_TEST_DB_NAME'])

        fb_client.login()
        self.cleanup_firebolt(fb_client)

        logging.info("Creating SETUP background table")
        fb_client.engine.run_sql(f"""CREATE {self.ci.configuration.parameters["table_type"]} 
        TABLE IF NOT EXISTS background_table 
        ( first_name text,last_name text,message text,pay int) primary index first_name;""")  # noqa

        logging.info("Ingesting SETUP Data")
        fb_client.engine.run_sql("""
        Insert into background_table VALUES ('Adam','Bako','Testing Firebolt', 300),
        ('Adama','Bakova','Hi Everyone', 150);""")  # noqa

        logging.info("Creating SETUP view")
        fb_client.engine.run_sql(f"""CREATE VIEW {self.ci.configuration.parameters["dbName"]} AS 
        SELECT * FROM background_table;""")  # noqa

    @staticmethod
    def random_with_n_digits(n: int) -> int:
        range_start = 10 ** (n - 1)
        range_end = (10 ** n) - 1
        return randint(range_start, range_end)

    def tearDown(self) -> None:
        """
        Executes after test
        """
        fb_client = FireboltClient(firebolt_user=os.environ['FIREBOLT_USER_NAME'],
                                   firebolt_pass=os.environ['FIREBOLT_PASSWORD'],
                                   database_name=os.environ['FIREBOLT_TEST_DB_NAME']
                                   )
        fb_client.login()

        self.cleanup_firebolt(fb_client)

    def cleanup_firebolt(self, fb_client):
        existing_indexes = fb_client.download_table_indexes()
        aggregation_indexes = [agg_index for agg_index in existing_indexes if agg_index["type"] == "aggregating"]
        join_indexes = [join_index for join_index in existing_indexes if join_index["type"] == "join"]

        views = fb_client.download_view_descriptions()
        tables = fb_client.download_table_descriptions()

        logging.info("Dropping Aggregation Indexes")
        for aggregation_index in aggregation_indexes:
            logging.info(f"Dropping {aggregation_index['index_name']}")
            fb_client.drop_aggregating_index(aggregation_index["index_name"])

        logging.info("Dropping Join Indexes")
        for join_index in join_indexes:
            logging.info(f"Dropping {join_index['index_name']}")
            fb_client.drop_join_index(join_index["index_name"])

        logging.info("Dropping Views")
        for view in views:
            logging.info(f"Dropping {view['table_name']}")
            fb_client.drop_view(view["table_name"])

        logging.info("Dropping Tables")
        for table in tables:
            logging.info(f"Dropping {table['table_name']}")
            fb_client.drop_table(table["table_name"])


class TestComponent(unittest.TestCase):

    def test_functional(self):
        warnings.filterwarnings(action="ignore", message="unclosed", category=ResourceWarning)
        functional_tests = DataDirTester(test_data_dir_class=FireboltWriterIntegrationTest)
        functional_tests.run()


if __name__ == "__main__":
    unittest.main()
