Firebolt Writer
=============

Firebolt is a cloud data warehouse for quick data analysis. 
The Firebolt Writer writes data to Firebolt tables and creates views for those tables.

**Table of contents:**

[TOC]

Functionality notes
===================

This writer works by creating views in Firebolt. There is always a background table along with a view.

For Full Load a new background table is uploaded, the view is deleted and recreated with the new background table. Then
the old background table is deleted.

For Incremental Load a new background table is uploaded, rows with primary keys not in the new background table 
are imported in from the old background table. The view is deleted and recreated with the new background table. Then
the old background table is deleted.

For Delete, the table with rows to delete is uploaded to firebolt (to_delete table). 
A new background table is created and rows with primary keys not in the to_delete background
table are imported in from the old background table. The view is deleted and recreated with the new background table. Then
the old background table is deleted.

Prerequisites
=============

Firebolt account / db setup.

Features
========

| **Feature**             | **Note**                                      |
|-------------------------|-----------------------------------------------|
| Custom UI               | Dynamic UI                              |
| Row Based configuration | Allows structuring the configuration in rows. |
| Incremental loading     | Allows fetching data in new increments.       |

Supported endpoints
===================

If you need more endpoints, please submit your request to
[ideas.keboola.com](https://ideas.keboola.com/)

Configuration
=============

### Parameters

- `"db"` - Connection settings
    - `"user"`- "example@example.com",
    - `"database"` -  Firebolt database name (default ingest engine will be used)
    - `"#password"`- Firebolt password
    - `"aws_api_key_id"` - OPT - AWS S3 key
    - `"#aws_api_key_secret"` - OPT - AWS S3 key
    - `"staging_bucket"`: - OPT - AWS S3 bucket

**NOTE**: AWS credentials may be left empty in AWS stack or provided as part of an `root.s3` parameter:

```json
"s3": {
    "isSliced": true,
    "region": "us-east-1",
    "bucket": "kbc-sapi-files",
    "key": "",
    "credentials": {
      "access_key_id": "CCCC",
      "secret_access_key": "AA..I+T",
      "session_token": "AAAA...POP"
    }
  }
```

- `"dbName"` - result firebolt db name
- `"table_type"`: `Fact` or `Dimension` - Firebolt table type
- `"loading_options"` - Object
    - `"load_type"`- one of`Full load` (overwrite result), `Append` (append data to existing table)
        
- `"items"` - List of column type mapping
    - `name` - source column name
      `dbName` - firebolt destination column name
      `type` -  e.g. `text`, `int`, `timestamp`
      `nullable` - boolean
      `size` - 

**Indexes**

- `primaryIndex` - Array of string column names
- `aggregation_indexes` - array of objects (only for)
    - example: 
    ```
  {
                "aggregations": [
                    {
                        "function": "MIN",
                        "column": "last_update_time"
                    },
                    {
                        "function": "MAX",
                        "column": "last_update_time"
                    }
                ],
                "index_name": "kbc_account",
                "key_columns": [
                    "account_id"
                ]
            }
  ```

### Sample configuration

```json
{
  "parameters": {
    "db": {
      "user": "example@example.com",
      "database": "test_db",
      "#password": "aaaaaaa",
      "aws_api_key_id": "AAAAAAA",
      "#aws_api_key_secret": "CCCCCCCC",
      "staging_bucket": "firebolt-test"
    },
        "items": [
            {
                "name": "account_id",
                "dbName": "account_id",
                "type": "VARCHAR",
                "nullable": false,
                "size": ""
            },
            {
                "name": "engine_id",
                "dbName": "engine_id",
                "type": "VARCHAR",
                "nullable": false,
                "size": ""
            },
            {
                "name": "id",
                "dbName": "id",
                "type": "VARCHAR",
                "nullable": false,
                "size": ""
            },
            {
                "name": "compute_provider_id",
                "dbName": "compute_provider_id",
                "type": "VARCHAR",
                "nullable": false,
                "size": ""
            },
            {
                "name": "compute_region_id",
                "dbName": "compute_region_id",
                "type": "VARCHAR",
                "nullable": false,
                "size": ""
            },
            {
                "name": "desired_status",
                "dbName": "desired_status",
                "type": "VARCHAR",
                "nullable": false,
                "size": ""
            }
        ],
        "aggregation_indexes": [
            {
                "aggregations": [
                    {
                        "function": "MIN",
                        "column": "last_update_time"
                    },
                    {
                        "function": "MAX",
                        "column": "last_update_time"
                    }
                ],
                "index_name": "kbc_account",
                "key_columns": [
                    "account_id"
                ]
            }
        ],
        "dbName": "core_engine_revisions",
        "tableId": "in.c-keboola-ex-aws-s3-753365763.core_engine_revisions",
        "table_type": "Fact",
        "join_indexes": [],
        "primaryIndex": [
            "account_id",
            "engine_id",
            "id"
        ],
        "incremental": false,
        "loading_options": {
            "load_type": "Full load"
        },
        "primaryKey": []
  },
  "image_parameters": {},
  "action": "run"
}
```




Post-Run scripts
======

You can specify SQL scripts that the engine in Firebolt will run after all tables and views are done being uploaded. 
Post run scripts are defined with the "post_run_scripts" parameter which is a dictionary that contains the "scripts" parameter
holding all the scripts in a list as strings. There are specific placeholder strings that can be used to replace table or view names, they are always 
in curly brackets {}:

{VIEW} - will be replaced as the name of the view created by the writer
{BACKGROUND_TABLE} - will be replaced as the name of the current background table created by the writer

you can also set specific object names to the scripts
{OBJECT_} - will be replaced as a specific object name described in the "custom_objects" dictionary
{OBJECT_DYNAMIC_} - will be replaced as a specific object name described in the "custom_objects" dictionary along with a unique id per run, so the name of the object is unique
```json

 "post_run_scripts": {
      "continue_on_failure": true,
      "scripts": [
        "SELECT * FROM {VIEW}",
        "SELECT * FROM {OBJECT_this}"
        "SELECT * FROM {OBJECT_DYNAMIC_that}"
      ],
      "custom_objects" : {
        "this" : "hello",
         "that" : "hi"
 }
    }

```

Development
-----------

If required, change local data folder (the `CUSTOM_FOLDER` placeholder) path to
your custom path in the docker-compose file:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    volumes:
      - ./:/code
      - ./CUSTOM_FOLDER:/data
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Clone this repository, init the workspace and run the component with following
command:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
git clone git clone https://bitbucket.org/kds_consulting_team/kds-team.wr-firebolt.git
cd kds-team.wr-firebolt
docker-compose build
docker-compose run --rm dev
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Run the test suite and lint check using this command:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
docker-compose run --rm test
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Integration
===========

For information about deployment and integration with KBC, please refer to the
[deployment section of developers
documentation](https://developers.keboola.com/extend/component/deployment/)
